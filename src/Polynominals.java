import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Polynominals {

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите степень многочленов");
        int exponentiation = Integer.parseInt(bufferedReader.readLine());

        Random random = new Random();

        /*
        создаем HashMap
        ключи и значения HashMap умножим друг на другу(получая одночлены)
        затем сложим произведения всех ключей и значений
        таким образом получим первый многочлен
         */
        HashMap<Integer, Integer> firstPolynominal = new HashMap<>();
        for (int i=0; i<3; i++) {
            firstPolynominal.put(random.nextInt(),random.nextInt());
        }

        //складываем одночлены из HashMap firstPolynominal
        int sumOfFirstPoly = 0;
        for (Map.Entry<Integer, Integer> pair: firstPolynominal.entrySet()) {
            sumOfFirstPoly += pair.getKey() * pair.getValue();
        }

        //создаем новый HashMap, ключи и значения которого будут одночленами
        HashMap<Integer, Integer> secondPolynominal = new HashMap<>();
        for (int i=0; i<2; i++) {
            secondPolynominal.put(random.nextInt(),random.nextInt());
        }

        //складываем одночлены из HashMap secondPolynominal
        int sumOfSecondPoly = 0;
        for (Map.Entry<Integer, Integer> pair: secondPolynominal.entrySet()) {
            sumOfSecondPoly += pair.getKey() + pair.getValue();
        }

        //Выводим в консоль сумму двух многочленов, возведенную в заданную степень
        System.out.println("Сумма 2 многочленов в степени " + exponentiation + " равна");
        System.out.println(Math.pow((sumOfFirstPoly+sumOfSecondPoly),exponentiation));
    }
}
